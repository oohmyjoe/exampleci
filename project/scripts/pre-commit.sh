echo "Running pre-commit hook"

echo "Running unittests"
./project/scripts/run-unittests.sh

if [ $? -ne 0 ]; then
    echo "Unit tests must pass before commit"
    exit 1
fi

echo "unittests succeeded"

echo "Run deployment tests"
